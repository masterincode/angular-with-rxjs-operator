## Angular Material Setup

    ng add @angular/material

## Add below line in Style.css file

- @import "~@angular/material/prebuilt-themes/indigo-pink.css";

## Open package.json file and check for below entry with same version, avoid mismatch of version 

- "@angular/cdk": "^9.2.4",
- "@angular/material": "^9.2.4"

## Open index.html file and below line, open code in IDE link will not appear here

- "<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">"

-  "<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono:300" rel="stylesheet">"