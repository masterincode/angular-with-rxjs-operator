export class Student {

    public fullName: string;
    public age: number;
    public city: string;

    constructor( fullName: string, age: number, city: string) {
        this.fullName = fullName;
        this.age = age;
        this.city = city;
    }
}
