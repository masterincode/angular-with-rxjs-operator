import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventPropertyComponent } from './component/rxjs-operator/range/binding/event-property/event-property.component';
import { RxjsOperatorSampleComponent } from './component/rxjs-operator/range/binding/event-property/rxjs-operator-sample/rxjs-operator-sample.component';
import { RangeComponent } from './component/rxjs-operator/range/range.component';


const routes: Routes = [
  { path: '',       component: RangeComponent  },
  { path: 'event-property', component: EventPropertyComponent},
  { path: 'rxjs-operator-sample', component: RxjsOperatorSampleComponent},
  { path: '**',     redirectTo: '/'  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
