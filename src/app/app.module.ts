import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RangeComponent } from './component/rxjs-operator/range/range.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EventPropertyComponent } from './component/rxjs-operator/range/binding/event-property/event-property.component';
import { ChildComponent } from './component/rxjs-operator/range/binding/event-property/child/child.component';
import { StudentComponent } from './model/student/student.component';
import { RxjsOperatorSampleComponent } from './component/rxjs-operator/range/binding/event-property/rxjs-operator-sample/rxjs-operator-sample.component';

@NgModule({
  declarations: [AppComponent, RangeComponent, EventPropertyComponent, ChildComponent, StudentComponent, RxjsOperatorSampleComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatGridListModule,
    FormsModule,
    MatTableModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
