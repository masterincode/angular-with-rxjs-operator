import { Component, OnInit } from '@angular/core';
import { range } from 'rxjs';

@Component({
  selector: 'app-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.css'],
})
export class RangeComponent implements OnInit {
  constructor() {}

  public firstArgument: number;
  public secondArgument: number;
  public resultList = [];

  ngOnInit(): void {}


  public calculate(): void {
    const rangeObservable = range(this.firstArgument, this.secondArgument);
    rangeObservable.subscribe((number) => {
      this.resultList.push(number);
    });
  }

  public reset(): void {
    this.firstArgument = 0;
    this.secondArgument = 0;
    this.resultList = [];
  }
}
