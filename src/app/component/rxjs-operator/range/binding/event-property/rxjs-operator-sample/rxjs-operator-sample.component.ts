import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from 'src/app/service/http-service.service';
import { filter, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-rxjs-operator-sample',
  templateUrl: './rxjs-operator-sample.component.html',
  styleUrls: ['./rxjs-operator-sample.component.css']
})
export class RxjsOperatorSampleComponent implements OnInit {

  constructor(private httpServiceService: HttpServiceService) { }

  ngOnInit(): void {

    this.httpServiceService.mapOperator()
    .pipe(
      map( response => response * 5 )
      ,filter( response => response % 2 == 0)
      
    )
    .subscribe(
      (next: any ) => {
        console.log('next Method: ',next);
      },
      (error: any ) => {
        console.error('error: ',error );
      },
      () => {
        console.warn('complete ');
      },
    );

    console.info('==========Combine Latest===========');
    combineLatest(
      this.httpServiceService.ofOperator(),
      this.httpServiceService.ofOperator()
     ).subscribe(
      ([ofResponse, mapReponse]: [any, any]) => {
        console.log('ofResponse: ',ofResponse, '   mapReponse ', mapReponse );
      },
      (error: any ) => {
        console.error('error: ',error );
      },
      () => {
        console.warn('complete ');
      },
    );


  }

}
