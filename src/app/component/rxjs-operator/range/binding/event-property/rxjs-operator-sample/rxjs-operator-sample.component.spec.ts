import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RxjsOperatorSampleComponent } from './rxjs-operator-sample.component';

describe('RxjsOperatorSampleComponent', () => {
  let component: RxjsOperatorSampleComponent;
  let fixture: ComponentFixture<RxjsOperatorSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RxjsOperatorSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RxjsOperatorSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
