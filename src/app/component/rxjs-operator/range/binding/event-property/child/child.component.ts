import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Student } from 'src/app/model/student';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor() { }

  @Input()
  public variableWithoutDefaultValue: string;
  @Input()
  public variableWithDefaultValue = 'Software Engineer';
  @Input()
  public student: Student;
  @Output()
  public studentEventEmitter =  new EventEmitter<Student>();


  ngOnInit(): void {

    if(this.variableWithDefaultValue) {
      console.log('if block');
    }
    else {
      console.log('else block');
    }
  }

  public sendDataToParent(): void {
     this.studentEventEmitter.emit(new Student('Sachin', 45, 'Mumbai'));
  }

  public changeFullName(name: string): void {
    this.student.fullName = name;
    this.studentEventEmitter.emit(new Student('Sachin', 45, 'Mumbai'));
  }

}
