import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Student } from 'src/app/model/student';
import { HttpServiceService } from 'src/app/service/http-service.service';

@Component({
  selector: 'app-event-property',
  templateUrl: './event-property.component.html',
  styleUrls: ['./event-property.component.css']
})
export class EventPropertyComponent implements OnInit {

  public student: Student; 
  public ngUnsubscribe =  new BehaviorSubject<any>('');
  constructor(private httpServiceService: HttpServiceService) { }

  ngOnInit(): void {
    this.student =  new Student('Sehwag', 25, 'Mumbai');

  }

  public name = 'Virendra Mishra';

  public defaultValue = ' Parent sending data to child ';

  public noDefaultValue = 'Mechanical Engineer';

  public setNewStudentData(newStudent: Student): void {
     this.student = newStudent;
  }

  public saveData(): void {
    this.httpServiceService.getData().subscribe(
      (next: any ) => {
        console.log('next');
      },
      (error: any ) => {
        console.error('next');
      },
      () => {
        console.warn('next');
      },
    );
  }

}
