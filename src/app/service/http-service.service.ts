import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, of, range } from 'rxjs';
import { Student } from '../model/student';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(private http: HttpClient) { }

  public getData(): Observable<Student> {
    return this.http.get<Student>('http://localhost:8080/testURL');
  }

  public ofOperator(): Observable<any> {
    return of([11, true, [1,2,3], 'Viru', 78,5, false ]);
  }

  public rangeOperator(): Observable<any> {
    return range(0,11);
  }

  public mapOperator(): Observable<number> {
    return from([1,2,3,4,5,6,7,8,9,10]);
  }

  public getName() {
    return 'Viru';
  }

  public getSurName() {
    return 'Mishra';
  }


}
